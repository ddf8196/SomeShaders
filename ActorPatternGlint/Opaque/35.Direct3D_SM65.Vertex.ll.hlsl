// shaderc command line:
//#line 1 "ActorPatternGlint__Opaque.Vertex"
//#line 87
struct Output
{
float4 gl_Position : SV_POSITION;

float4 v_color0 : COLOR0;

float4 v_fog : COLOR2;

float4 v_layerUv : TEXCOORD3;

float4 v_light : COLOR3;

centroid float2 v_texcoord0 : TEXCOORD0;

centroid float4 v_texcoords : TEXCOORD2;

};
//#line 123
struct NoopSampler
{
int noop;
};
//#line 82
float intBitsToFloat(int _x) { return asfloat(_x); }
float2 intBitsToFloat(uint2 _x) { return asfloat(_x); }
float3 intBitsToFloat(uint3 _x) { return asfloat(_x); }
float4 intBitsToFloat(uint4 _x) { return asfloat(_x); }


float uintBitsToFloat(uint _x) { return asfloat(_x); }
float2 uintBitsToFloat(uint2 _x) { return asfloat(_x); }
float3 uintBitsToFloat(uint3 _x) { return asfloat(_x); }
float4 uintBitsToFloat(uint4 _x) { return asfloat(_x); }

uint floatBitsToUint(float _x) { return asuint(_x); }
uint2 floatBitsToUint(float2 _x) { return asuint(_x); }
uint3 floatBitsToUint(float3 _x) { return asuint(_x); }
uint4 floatBitsToUint(float4 _x) { return asuint(_x); }

int floatBitsToInt(float _x) { return asint(_x); }
int2 floatBitsToInt(float2 _x) { return asint(_x); }
int3 floatBitsToInt(float3 _x) { return asint(_x); }
int4 floatBitsToInt(float4 _x) { return asint(_x); }

uint bitfieldReverse(uint _x) { return reversebits(_x); }
uint2 bitfieldReverse(uint2 _x) { return reversebits(_x); }
uint3 bitfieldReverse(uint3 _x) { return reversebits(_x); }
uint4 bitfieldReverse(uint4 _x) { return reversebits(_x); }

uint packHalf2x16(float2 _x)
{
return (f32tof16(_x.y)<<16) | f32tof16(_x.x);
}

float2 unpackHalf2x16(uint _x)
{
return float2(f16tof32(_x & 0xffff), f16tof32(_x >> 16) );
}

struct BgfxSampler2D
{
SamplerState m_sampler;
Texture2D m_texture;
};

struct BgfxISampler2D
{
Texture2D<int4> m_texture;
};

struct BgfxUSampler2D
{
Texture2D<uint4> m_texture;
};

struct BgfxSampler2DArray
{
SamplerState m_sampler;
Texture2DArray m_texture;
};

struct BgfxSampler2DShadow
{
SamplerComparisonState m_sampler;
Texture2D m_texture;
};

struct BgfxSampler2DArrayShadow
{
SamplerComparisonState m_sampler;
Texture2DArray m_texture;
};

struct BgfxSampler3D
{
SamplerState m_sampler;
Texture3D m_texture;
};

struct BgfxISampler3D
{
Texture3D<int4> m_texture;
};

struct BgfxUSampler3D
{
Texture3D<uint4> m_texture;
};

struct BgfxSamplerCube
{
SamplerState m_sampler;
TextureCube m_texture;
};

struct BgfxSampler2DMS
{
Texture2DMS<float4> m_texture;
};

struct BgfxSamplerState
{
SamplerState m_samplerState;
};

struct BgfxSamplerComparisonState
{
SamplerComparisonState m_samplerState;
};

float4 bgfxTexture2D(BgfxSampler2D _sampler, float2 _coord)
{
return _sampler.m_texture.Sample(_sampler.m_sampler, _coord);
}

float4 bgfxTexture2DLod(BgfxSampler2D _sampler, float2 _coord, float _level)
{
return _sampler.m_texture.SampleLevel(_sampler.m_sampler, _coord, _level);
}

float4 bgfxTexture2DProj(BgfxSampler2D _sampler, float3 _coord)
{
float2 coord = _coord.xy * rcp(_coord.z);
return _sampler.m_texture.Sample(_sampler.m_sampler, coord);
}

float4 bgfxTexture2DProj(BgfxSampler2D _sampler, float4 _coord)
{
float2 coord = _coord.xy * rcp(_coord.w);
return _sampler.m_texture.Sample(_sampler.m_sampler, coord);
}

float4 bgfxTexture2DGrad(BgfxSampler2D _sampler, float2 _coord, float2 _dPdx, float2 _dPdy)
{
return _sampler.m_texture.SampleGrad(_sampler.m_sampler, _coord, _dPdx, _dPdy);
}

float4 bgfxTexture2DArray(BgfxSampler2DArray _sampler, float3 _coord)
{
return _sampler.m_texture.Sample(_sampler.m_sampler, _coord);
}

float4 bgfxTexture2DArrayLod(BgfxSampler2DArray _sampler, float3 _coord, float _lod)
{
return _sampler.m_texture.SampleLevel(_sampler.m_sampler, _coord, _lod);
}

float bgfxShadow2D(BgfxSampler2DShadow _sampler, float3 _coord)
{
return _sampler.m_texture.SampleCmpLevelZero(_sampler.m_sampler, _coord.xy, _coord.z);
}

float4 bgfxShadow2DArray(BgfxSampler2DArrayShadow _sampler, float4 _coord)
{
return _sampler.m_texture.SampleCmpLevelZero(_sampler.m_sampler, _coord.xyz, _coord.w);
}

float bgfxShadow2DProj(BgfxSampler2DShadow _sampler, float4 _coord)
{
float3 coord = _coord.xyz * rcp(_coord.w);
return _sampler.m_texture.SampleCmpLevelZero(_sampler.m_sampler, coord.xy, coord.z);
}

float4 bgfxTexture3D(BgfxSampler3D _sampler, float3 _coord)
{
return _sampler.m_texture.Sample(_sampler.m_sampler, _coord);
}

float4 bgfxTexture3DLod(BgfxSampler3D _sampler, float3 _coord, float _level)
{
return _sampler.m_texture.SampleLevel(_sampler.m_sampler, _coord, _level);
}

int4 bgfxTexture3D(BgfxISampler3D _sampler, float3 _coord)
{
uint3 size;
_sampler.m_texture.GetDimensions(size.x, size.y, size.z);
return _sampler.m_texture.Load(int4(_coord * size, 0) );
}

uint4 bgfxTexture3D(BgfxUSampler3D _sampler, float3 _coord)
{
uint3 size;
_sampler.m_texture.GetDimensions(size.x, size.y, size.z);
return _sampler.m_texture.Load(int4(_coord * size, 0) );
}

float4 bgfxTextureCube(BgfxSamplerCube _sampler, float3 _coord)
{
return _sampler.m_texture.Sample(_sampler.m_sampler, _coord);
}

float4 bgfxTextureCubeLod(BgfxSamplerCube _sampler, float3 _coord, float _level)
{
return _sampler.m_texture.SampleLevel(_sampler.m_sampler, _coord, _level);
}

float4 bgfxTexelFetch(BgfxSampler2D _sampler, int2 _coord, int _lod)
{
return _sampler.m_texture.Load(int3(_coord, _lod) );
}

float2 bgfxTextureSize(BgfxSampler2D _sampler, int _lod)
{
float2 result;
_sampler.m_texture.GetDimensions(result.x, result.y);
return result;
}

int4 bgfxTexelFetch(BgfxISampler2D _sampler, int2 _coord, int _lod)
{
return _sampler.m_texture.Load(int3(_coord, _lod) );
}

uint4 bgfxTexelFetch(BgfxUSampler2D _sampler, int2 _coord, int _lod)
{
return _sampler.m_texture.Load(int3(_coord, _lod) );
}

float4 bgfxTexelFetch(BgfxSampler2DMS _sampler, int2 _coord, int _sampleIdx)
{
return _sampler.m_texture.Load(_coord, _sampleIdx);
}

float4 bgfxTexelFetch(BgfxSampler3D _sampler, int3 _coord, int _lod)
{
return _sampler.m_texture.Load(int4(_coord, _lod) );
}

float3 bgfxTextureSize(BgfxSampler3D _sampler, int _lod)
{
float3 result;
_sampler.m_texture.GetDimensions(result.x, result.y, result.z);
return result;
}

float4 bgfxTextureGather(BgfxSampler2D _sampler, float2 _coord, int comp)
{
if (comp == 0)
{
return _sampler.m_texture.GatherRed(_sampler.m_sampler, _coord);
}
else if (comp == 1)
{
return _sampler.m_texture.GatherGreen(_sampler.m_sampler, _coord);
}
else if (comp == 2)
{
return _sampler.m_texture.GatherBlue(_sampler.m_sampler, _coord);
}
else if (comp == 3)
{
return _sampler.m_texture.GatherAlpha(_sampler.m_sampler, _coord);
}
return float4(0, 0, 0, 0);
}
//#line 345
float4 bgfxTexture2D(NoopSampler _sampler, float2 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DLod(NoopSampler _sampler, float2 _coord, float _level) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DProj(NoopSampler _sampler, float3 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DProj(NoopSampler _sampler, float4 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DGrad(NoopSampler _sampler, float2 _coord, float2 _dPdx, float2 _dPdy) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DArray(NoopSampler _sampler, float3 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture2DArrayLod(NoopSampler _sampler, float3 _coord, float _lod) { return float4(0.0,0.0,0.0,0.0);}
float bgfxShadow2D(NoopSampler _sampler, float3 _coord) { return 0.0;}
float bgfxShadow2DProj(NoopSampler _sampler, float4 _coord) { return 0.0;}
float4 bgfxTexture3D(NoopSampler _sampler, float3 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexture3DLod(NoopSampler _sampler, float3 _coord, float _level) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTextureCube(NoopSampler _sampler, float3 _coord) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTextureCubeLod(NoopSampler _sampler, float3 _coord, float _level) { return float4(0.0,0.0,0.0,0.0);}
float4 bgfxTexelFetch(NoopSampler _sampler, int2 _coord, int _lod) { return float4(0.0,0.0,0.0,0.0);}
float2 bgfxTextureSize(NoopSampler _sampler, int _lod) { return float2(0.0,0.0);}
float4 bgfxTextureGather(NoopSampler _sampler, float2 _coord, int comp) { return float4(0.0,0.0,0.0,0.0);}
NoopSampler getSampler(NoopSampler _sampler, BgfxSamplerState _samplerState) { return _sampler;}
NoopSampler getSampler(NoopSampler _sampler, NoopSampler _samplerState) { return _sampler;}
//#line 437
BgfxSampler2D getSampler(Texture2D _texture, BgfxSamplerState _samplerState) { BgfxSampler2D bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 438
BgfxSampler2DShadow getSampler(Texture2D _texture, BgfxSamplerComparisonState _samplerState) { BgfxSampler2DShadow bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 439
BgfxSampler2DArrayShadow getSampler(Texture2DArray _texture, BgfxSamplerComparisonState _samplerState) { BgfxSampler2DArrayShadow bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 440
BgfxSampler2DArray getSampler(Texture2DArray _texture, BgfxSamplerState _samplerState) { BgfxSampler2DArray bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 441
BgfxSampler3D getSampler(Texture3D _texture, BgfxSamplerState _samplerState) { BgfxSampler3D bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 442
BgfxSamplerCube getSampler(TextureCube _texture, BgfxSamplerState _samplerState) { BgfxSamplerCube bgfxSampler; bgfxSampler.m_texture = _texture; bgfxSampler.m_sampler = _samplerState.m_samplerState; return bgfxSampler; }
//#line 645
float3 instMul(float3 _vec, float3x3 _mtx) { return mul(_mtx, _vec); }
float3 instMul(float3x3 _mtx, float3 _vec) { return mul(_vec, _mtx); }
float4 instMul(float4 _vec, float4x4 _mtx) { return mul(_mtx, _vec); }
float4 instMul(float4x4 _mtx, float4 _vec) { return mul(_vec, _mtx); }

bool2 lessThan(float2 _a, float2 _b) { return _a < _b; }
bool3 lessThan(float3 _a, float3 _b) { return _a < _b; }
bool4 lessThan(float4 _a, float4 _b) { return _a < _b; }

bool2 lessThanEqual(float2 _a, float2 _b) { return _a <= _b; }
bool3 lessThanEqual(float3 _a, float3 _b) { return _a <= _b; }
bool4 lessThanEqual(float4 _a, float4 _b) { return _a <= _b; }

bool2 greaterThan(float2 _a, float2 _b) { return _a > _b; }
bool3 greaterThan(float3 _a, float3 _b) { return _a > _b; }
bool4 greaterThan(float4 _a, float4 _b) { return _a > _b; }

bool2 greaterThanEqual(float2 _a, float2 _b) { return _a >= _b; }
bool3 greaterThanEqual(float3 _a, float3 _b) { return _a >= _b; }
bool4 greaterThanEqual(float4 _a, float4 _b) { return _a >= _b; }

bool2 notEqual(float2 _a, float2 _b) { return _a != _b; }
bool3 notEqual(float3 _a, float3 _b) { return _a != _b; }
bool4 notEqual(float4 _a, float4 _b) { return _a != _b; }

bool2 equal(float2 _a, float2 _b) { return _a == _b; }
bool3 equal(float3 _a, float3 _b) { return _a == _b; }
bool4 equal(float4 _a, float4 _b) { return _a == _b; }

float mix(float _a, float _b, float _t) { return lerp(_a, _b, _t); }
float2 mix(float2 _a, float2 _b, float2 _t) { return lerp(_a, _b, _t); }
float3 mix(float3 _a, float3 _b, float3 _t) { return lerp(_a, _b, _t); }
float4 mix(float4 _a, float4 _b, float4 _t) { return lerp(_a, _b, _t); }

float mod(float _a, float _b) { return _a - _b * floor(_a / _b); }
float2 mod(float2 _a, float2 _b) { return _a - _b * floor(_a / _b); }
float3 mod(float3 _a, float3 _b) { return _a - _b * floor(_a / _b); }
float4 mod(float4 _a, float4 _b) { return _a - _b * floor(_a / _b); }
//#line 806
float2 vec2_splat(float _x) { return float2(_x, _x); }
float3 vec3_splat(float _x) { return float3(_x, _x, _x); }
float4 vec4_splat(float _x) { return float4(_x, _x, _x, _x); }


uint2 uvec2_splat(uint _x) { return uint2(_x, _x); }
uint3 uvec3_splat(uint _x) { return uint3(_x, _x, _x); }
uint4 uvec4_splat(uint _x) { return uint4(_x, _x, _x, _x); }


float4x4 mtxFromRows(float4 _0, float4 _1, float4 _2, float4 _3)
{



return float4x4(_0, _1, _2, _3);

}

float4x4 mtxFromCols(float4 _0, float4 _1, float4 _2, float4 _3)
{



return transpose(float4x4(_0, _1, _2, _3) );

}

float3x3 mtxFromRows(float3 _0, float3 _1, float3 _2)
{



return float3x3(_0, _1, _2);

}

float3x3 mtxFromCols(float3 _0, float3 _1, float3 _2)
{



return transpose(float3x3(_0, _1, _2) );

}


float trunc(float _f)
{
return float(int(_f));
}
//#line 899
float fmod(float _a, float _b) { return _a - _b * trunc(_a / _b); }
float2 fmod(float2 _a, float2 _b) { return float2(fmod(_a.x,_b.x), fmod(_a.y,_b.y)); }
float3 fmod(float3 _a, float3 _b) { return float3(fmod(_a.x,_b.x), fmod(_a.y,_b.y), fmod(_a.z,_b.z)); }
float4 fmod(float4 _a, float4 _b) { return float4(fmod(_a.x,_b.x), fmod(_a.y,_b.y), fmod(_a.z,_b.z), fmod(_a.w,_b.w)); }

//#line 11 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Core/bgfx_compute.dragonh"
struct NoopImage2D
{
int noop;
};
struct NoopImage3D
{
int noop;
};
//#line 368
struct BgfxROImage2D_rg16f { Texture2D<float2> m_texture; }; struct BgfxRWImage2D_rg16f { RWTexture2D<float2> m_texture; }; struct BgfxROImage2DArray_rg16f { Texture2DArray<float2> m_texture; }; struct BgfxRWImage2DArray_rg16f { RWTexture2DArray<float2> m_texture; }; struct BgfxROImage3D_rg16f { Texture3D<float2> m_texture; }; struct BgfxRWImage3D_rg16f { RWTexture3D<float2> m_texture; }; float4 imageLoad(BgfxROImage2D_rg16f _image, int2 _uv) { return _image.m_texture[_uv].xyyy; } int2 imageSize(BgfxROImage2D_rg16f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } float4 imageLoad(BgfxRWImage2D_rg16f _image, int2 _uv) { return _image.m_texture[_uv].xyyy; } int2 imageSize(BgfxRWImage2D_rg16f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_rg16f _image, int2 _uv, float4 _value) { _image.m_texture[_uv] = _value.xy; } float4 imageLoad(BgfxROImage2DArray_rg16f _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxROImage2DArray_rg16f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage2DArray_rg16f _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } void imageStore(BgfxRWImage2DArray_rg16f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.xy; } int3 imageSize(BgfxRWImage2DArray_rg16f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxROImage3D_rg16f _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxROImage3D_rg16f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage3D_rg16f _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxRWImage3D_rg16f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_rg16f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.xy; }
//#line 370
struct BgfxROImage2D_rgba8 { Texture2D<unorm float4> m_texture; }; struct BgfxRWImage2D_rgba8 { RWTexture2D<unorm float4> m_texture; }; struct BgfxROImage2DArray_rgba8 { Texture2DArray<unorm float4> m_texture; }; struct BgfxRWImage2DArray_rgba8 { RWTexture2DArray<unorm float4> m_texture; }; struct BgfxROImage3D_rgba8 { Texture3D<unorm float4> m_texture; }; struct BgfxRWImage3D_rgba8 { RWTexture3D<unorm float4> m_texture; };
//#line 375
struct BgfxROImage2D_rgba16f { Texture2D<float4> m_texture; }; struct BgfxRWImage2D_rgba16f { RWTexture2D<float4> m_texture; }; struct BgfxROImage2DArray_rgba16f { Texture2DArray<float4> m_texture; }; struct BgfxRWImage2DArray_rgba16f { RWTexture2DArray<float4> m_texture; }; struct BgfxROImage3D_rgba16f { Texture3D<float4> m_texture; }; struct BgfxRWImage3D_rgba16f { RWTexture3D<float4> m_texture; };
//#line 379
struct BgfxROImage2D_r32f { Texture2D<float> m_texture; }; struct BgfxRWImage2D_r32f { RWTexture2D<float> m_texture; }; struct BgfxROImage2DArray_r32f { Texture2DArray<float> m_texture; }; struct BgfxRWImage2DArray_r32f { RWTexture2DArray<float> m_texture; }; struct BgfxROImage3D_r32f { Texture3D<float> m_texture; }; struct BgfxRWImage3D_r32f { RWTexture3D<float> m_texture; }; float4 imageLoad(BgfxROImage2D_r32f _image, int2 _uv) { return _image.m_texture[_uv].xxxx; } int2 imageSize(BgfxROImage2D_r32f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } float4 imageLoad(BgfxRWImage2D_r32f _image, int2 _uv) { return _image.m_texture[_uv].xxxx; } int2 imageSize(BgfxRWImage2D_r32f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_r32f _image, int2 _uv, float4 _value) { _image.m_texture[_uv] = _value.x; } float4 imageLoad(BgfxROImage2DArray_r32f _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxROImage2DArray_r32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage2DArray_r32f _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } void imageStore(BgfxRWImage2DArray_r32f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.x; } int3 imageSize(BgfxRWImage2DArray_r32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxROImage3D_r32f _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxROImage3D_r32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage3D_r32f _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxRWImage3D_r32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_r32f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.x; }
//#line 380
struct BgfxROImage2D_rgba32f { Texture2D<float4> m_texture; }; struct BgfxRWImage2D_rgba32f { RWTexture2D<float4> m_texture; }; struct BgfxROImage2DArray_rgba32f { Texture2DArray<float4> m_texture; }; struct BgfxRWImage2DArray_rgba32f { RWTexture2DArray<float4> m_texture; }; struct BgfxROImage3D_rgba32f { Texture3D<float4> m_texture; }; struct BgfxRWImage3D_rgba32f { RWTexture3D<float4> m_texture; }; float4 imageLoad(BgfxROImage2D_rgba32f _image, int2 _uv) { return _image.m_texture[_uv].xyzw; } int2 imageSize(BgfxROImage2D_rgba32f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } float4 imageLoad(BgfxRWImage2D_rgba32f _image, int2 _uv) { return _image.m_texture[_uv].xyzw; } int2 imageSize(BgfxRWImage2D_rgba32f _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_rgba32f _image, int2 _uv, float4 _value) { _image.m_texture[_uv] = _value.xyzw; } float4 imageLoad(BgfxROImage2DArray_rgba32f _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxROImage2DArray_rgba32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage2DArray_rgba32f _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } void imageStore(BgfxRWImage2DArray_rgba32f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.xyzw; } int3 imageSize(BgfxRWImage2DArray_rgba32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxROImage3D_rgba32f _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxROImage3D_rgba32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } float4 imageLoad(BgfxRWImage3D_rgba32f _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxRWImage3D_rgba32f _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_rgba32f _image, int3 _uvw, float4 _value) { _image.m_texture[_uvw] = _value.xyzw; }
//#line 381
struct BgfxROImage2D_r32ui { Texture2D<uint> m_texture; }; struct BgfxRWImage2D_r32ui { RWTexture2D<uint> m_texture; }; struct BgfxROImage2DArray_r32ui { Texture2DArray<uint> m_texture; }; struct BgfxRWImage2DArray_r32ui { RWTexture2DArray<uint> m_texture; }; struct BgfxROImage3D_r32ui { Texture3D<uint> m_texture; }; struct BgfxRWImage3D_r32ui { RWTexture3D<uint> m_texture; }; uint4 imageLoad(BgfxROImage2D_r32ui _image, int2 _uv) { return _image.m_texture[_uv].xxxx; } int2 imageSize(BgfxROImage2D_r32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } uint4 imageLoad(BgfxRWImage2D_r32ui _image, int2 _uv) { return _image.m_texture[_uv].xxxx; } int2 imageSize(BgfxRWImage2D_r32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_r32ui _image, int2 _uv, uint4 _value) { _image.m_texture[_uv] = _value.x; } uint4 imageLoad(BgfxROImage2DArray_r32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxROImage2DArray_r32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage2DArray_r32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } void imageStore(BgfxRWImage2DArray_r32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.x; } int3 imageSize(BgfxRWImage2DArray_r32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxROImage3D_r32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxROImage3D_r32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage3D_r32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xxxx; } int3 imageSize(BgfxRWImage3D_r32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_r32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.x; }
//#line 382
struct BgfxROImage2D_rg32ui { Texture2D<uint2> m_texture; }; struct BgfxRWImage2D_rg32ui { RWTexture2D<uint2> m_texture; }; struct BgfxROImage2DArray_rg32ui { Texture2DArray<uint2> m_texture; }; struct BgfxRWImage2DArray_rg32ui { RWTexture2DArray<uint2> m_texture; }; struct BgfxROImage3D_rg32ui { Texture3D<uint2> m_texture; }; struct BgfxRWImage3D_rg32ui { RWTexture3D<uint2> m_texture; }; uint4 imageLoad(BgfxROImage2D_rg32ui _image, int2 _uv) { return _image.m_texture[_uv].xyyy; } int2 imageSize(BgfxROImage2D_rg32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } uint4 imageLoad(BgfxRWImage2D_rg32ui _image, int2 _uv) { return _image.m_texture[_uv].xyyy; } int2 imageSize(BgfxRWImage2D_rg32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_rg32ui _image, int2 _uv, uint4 _value) { _image.m_texture[_uv] = _value.xy; } uint4 imageLoad(BgfxROImage2DArray_rg32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxROImage2DArray_rg32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage2DArray_rg32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } void imageStore(BgfxRWImage2DArray_rg32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.xy; } int3 imageSize(BgfxRWImage2DArray_rg32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxROImage3D_rg32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxROImage3D_rg32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage3D_rg32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyyy; } int3 imageSize(BgfxRWImage3D_rg32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_rg32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.xy; }
//#line 383
struct BgfxROImage2D_rgba32ui { Texture2D<uint4> m_texture; }; struct BgfxRWImage2D_rgba32ui { RWTexture2D<uint4> m_texture; }; struct BgfxROImage2DArray_rgba32ui { Texture2DArray<uint4> m_texture; }; struct BgfxRWImage2DArray_rgba32ui { RWTexture2DArray<uint4> m_texture; }; struct BgfxROImage3D_rgba32ui { Texture3D<uint4> m_texture; }; struct BgfxRWImage3D_rgba32ui { RWTexture3D<uint4> m_texture; }; uint4 imageLoad(BgfxROImage2D_rgba32ui _image, int2 _uv) { return _image.m_texture[_uv].xyzw; } int2 imageSize(BgfxROImage2D_rgba32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } uint4 imageLoad(BgfxRWImage2D_rgba32ui _image, int2 _uv) { return _image.m_texture[_uv].xyzw; } int2 imageSize(BgfxRWImage2D_rgba32ui _image) { uint2 result; _image.m_texture.GetDimensions(result.x, result.y); return int2(result); } void imageStore(BgfxRWImage2D_rgba32ui _image, int2 _uv, uint4 _value) { _image.m_texture[_uv] = _value.xyzw; } uint4 imageLoad(BgfxROImage2DArray_rgba32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxROImage2DArray_rgba32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage2DArray_rgba32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } void imageStore(BgfxRWImage2DArray_rgba32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.xyzw; } int3 imageSize(BgfxRWImage2DArray_rgba32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxROImage3D_rgba32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxROImage3D_rgba32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } uint4 imageLoad(BgfxRWImage3D_rgba32ui _image, int3 _uvw) { return _image.m_texture[_uvw].xyzw; } int3 imageSize(BgfxRWImage3D_rgba32ui _image) { uint3 result; _image.m_texture.GetDimensions(result.x, result.y, result.z); return int3(result); } void imageStore(BgfxRWImage3D_rgba32ui _image, int3 _uvw, uint4 _value) { _image.m_texture[_uvw] = _value.xyzw; }

//#line 29 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Core/bgfx_raytracing.dragonh"
void rayQueryInitializeKHR(
inout RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q,
in RaytracingAccelerationStructure topLevel,
uint rayFlags,
uint cullMask,
float3 origin,
float tMin,
float3 direction,
float tMax
) {
RayDesc ray;
ray.TMin = tMin;
ray.TMax = tMax;
ray.Origin = origin;
ray.Direction = direction;

q.TraceRayInline(topLevel, RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES | rayFlags, cullMask, ray);
}

bool rayQueryProceedKHR(inout RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q) {
return q.Proceed();
}

uint rayQueryGetIntersectionTypeKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q, bool committed) {
if(committed) {
return q.CommittedStatus();
} else {
return q.CandidateType();
}
}

void rayQueryConfirmIntersectionKHR(inout RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q) {
q.CommitNonOpaqueTriangleHit();
}

int rayQueryGetIntersectionPrimitiveIndexKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q, bool committed) {
if(committed) {
return q.CommittedPrimitiveIndex();
} else {
return q.CandidatePrimitiveIndex();
}
}

float2 rayQueryGetIntersectionBarycentricsKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q, bool committed) {
if(committed) {
return q.CommittedTriangleBarycentrics();
} else {
return q.CandidateTriangleBarycentrics();
}
}

float rayQueryGetIntersectionTKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q, bool committed) {
return q.CommittedRayT();
}

float3 rayQueryGetWorldRayOriginKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q) {
return q.WorldRayOrigin();
}

float3 rayQueryGetWorldRayDirectionKHR(RayQuery<RAY_FLAG_SKIP_PROCEDURAL_PRIMITIVES> q) {
return q.WorldRayDirection();
}

//#line 15 "ActorPatternGlint__OpaqueChange_Color_Off__Emissive_Off__Fancy_Off__Instancing_Off__MaskedMultitexture_Off__Tinting_Enabled__UIEntity_Enabled__"
cbuffer VertexUniforms : register(b0) {

static float4 u_viewRect;
uniform float4x4 u_proj;
static float4 UseAlphaRewrite;

uniform float4x4 u_view;
static float4 PatternCount;

static float4 ChangeColor;
uniform float4 FogControl;
static float4 u_viewTexel;
static float4x4 u_invView;
static float4x4 u_invProj;
static float4x4 u_viewProj;
uniform float4 OverlayColor;
static float4x4 u_invViewProj;
uniform float4x4 u_model[4];

static float4x4 u_modelView;
static float4x4 u_modelViewProj;
static float4 u_alphaRef4;

static float4 LightWorldSpaceDirection;
uniform float4 UVAnimation;
uniform float4 UVScale;
static float4 LightDiffuseColorAndIlluminance;
static float4 ColorBased;

uniform float4 SubPixelOffset;
static float4 TintedAlphaTestEnabled;

static float4 HudOpacity;

uniform float4 FogColor;
static float4 ActorFPEpsilon;

static float4 MatColor;
static float4 MultiplicativeTintColor;
uniform float4 TileLightColor;
uniform float4x4 Bones[8];
static float4 PatternColors[7];
static float4 PatternUVOffsetsAndScales[7];
static float4 GlintColor;


};

cbuffer FragmentUniforms : register(b1) {
//#line 109
};

static float4 ViewRect;
static float4x4 Proj;

static float4x4 View;



static float4 ViewTexel;
static float4x4 InvView;
static float4x4 InvProj;
static float4x4 ViewProj;

static float4x4 InvViewProj;
static float4x4 WorldArray[4];
static float4x4 World;
static float4x4 WorldView;
static float4x4 WorldViewProj;
static float4 AlphaRef4;
static float AlphaRef;
//#line 151
struct VertexInput {
float3 position;
int boneId;
float4 normal;
float2 texcoord0;
float4 color0;
//#line 163
};

struct VertexOutput {
float4 position;
float2 texcoord0;
float4 color0;
float4 texcoords;
float4 layerUv;
float4 light;
float4 fog;
};

struct FragmentInput {
float2 texcoord0;
float4 color0;
float4 texcoords;
float4 layerUv;
float4 light;
float4 fog;
};

struct FragmentOutput {
float4 Color0;
};


uniform SamplerState s_MatTextureSampler : register(s0); uniform Texture2D s_MatTextureTexture : register(t0); static BgfxSampler2D s_MatTexture = { s_MatTextureSampler, s_MatTextureTexture };
//#line 192
uniform SamplerState s_MatTexture1Sampler : register(s1); uniform Texture2D s_MatTexture1Texture : register(t1); static BgfxSampler2D s_MatTexture1 = { s_MatTexture1Sampler, s_MatTexture1Texture };
//#line 195
uniform SamplerState s_MatTexture2Sampler : register(s2); uniform Texture2D s_MatTexture2Texture : register(t2); static BgfxSampler2D s_MatTexture2 = { s_MatTexture2Sampler, s_MatTexture2Texture };

//#line 8 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Internal/Structs/InputOutput.dragonh"
struct StandardSurfaceInput {
float2 UV;
float3 Color;
float Alpha;

float4 layerUv;
float4 texcoords;
float2 texcoord0;
float4 light;
float4 fog;

};

struct StandardVertexInput {
VertexInput vertInput;

float3 worldPos;
};

StandardSurfaceInput StandardTemplate_DefaultInput(FragmentInput fragInput) {
StandardSurfaceInput result;

result.UV = float2(0,0);
result.Color = float3(1,1,1);
result.Alpha = 1.0;

result.layerUv = fragInput.layerUv;
result.texcoords = fragInput.texcoords;
result.texcoord0 = fragInput.texcoord0;
result.light = fragInput.light;
result.fog = fragInput.fog;

return result;
}

struct StandardSurfaceOutput {
float3 Albedo;
float Alpha;

float Metallic;
float Roughness;
float Occlusion;
float Emissive;
float3 AmbientLight;

float3 ViewSpaceNormal;
};

StandardSurfaceOutput StandardTemplate_DefaultOutput() {
StandardSurfaceOutput result;

result.Albedo = float3(1,1,1);
result.Alpha = 1.0;

result.Metallic = 0.0;
result.Roughness = 1.0;
result.Occlusion = 0.0;
result.Emissive = 0.0;
result.AmbientLight = float3(0.0, 0.0, 0.0);
result.ViewSpaceNormal = float3(0, 1, 0);

return result;
}

//#line 1 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/TAAUtil.dragonh"
float4 jitterVertexPosition(float3 worldPosition){
float4x4 offsetProj = Proj;
//#line 8
offsetProj[0][2] += SubPixelOffset.x;
offsetProj[1][2] -= SubPixelOffset.y;

return mul(offsetProj, mul(View, float4(worldPosition, 1.0f)));
}

//#line 3 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/DynamicUtil.dragonh"
float2 applyUvAnimation(float2 uv, const float4 uvAnimation) {
uv = uvAnimation.xy + (uv * uvAnimation.zw);
return uv;
}

float4 applyOverlayColor(float4 diffuse, const float4 overlayColor) {
diffuse.rgb = mix(diffuse.rgb, overlayColor.rgb, overlayColor.a);

return diffuse;
}

float4 applyColorChange(float4 originalColor, float4 changeColor, float alpha) {
originalColor.rgb = mix(originalColor, originalColor*changeColor, alpha).rgb;
return originalColor;
}

float4 applyMultiColorChange(float4 diffuse, float3 changeColor, float3 multiplicativeTintColor) {

float2 colorMask = diffuse.rg;


diffuse.rgb = colorMask.rrr * changeColor;


diffuse.rgb = mix(diffuse.rgb, colorMask.ggg * multiplicativeTintColor.rgb, ceil(colorMask.g));

return diffuse;
}

float4 applyLighting(float4 diffuse, const float4 light) {
diffuse.rgb *= light.rgb;

return diffuse;
}

float calculateLightIntensity(const float4x4 world, const float4 normal, const float4 tileLightColor) {
//#line 50
return 1.0;

}

//#line 5 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/ActorUtil.dragonh"
float4 applyChangeColor(float4 diffuse, float4 changeColor, float3 multiplicativeTintColor, float shouldChangeAlpha) {
//#line 18
diffuse.a = max(shouldChangeAlpha, diffuse.a);

return diffuse;
}

float4 applyEmissiveLighting(float4 diffuse, float4 light) {
//#line 27
diffuse = applyLighting(diffuse, light);

return diffuse;
}

bool shouldDiscard(float3 diffuse, float alpha, float epsilon) {
bool result = false;
//#line 48
result = alpha < 0.5;
//#line 55
return result;
}

float4 getActorAlbedoNoColorChange(float2 uv) {
float4 albedo = MatColor;
albedo *= bgfxTexture2D(s_MatTexture, uv);
//#line 69
return albedo;
}

float4 applyActorDiffuse(float4 albedo, float3 color, float4 light) {
albedo.rgb *= mix(float3(1, 1, 1), color, ColorBased.x);

albedo = applyOverlayColor(albedo, OverlayColor);
albedo = applyEmissiveLighting(albedo, light);

return albedo;
}

//#line 8 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/FogUtil.dragonh"
float calculateFogIntensity(float cameraDepth, float maxDistance, float fogStart, float fogEnd) {
float distance = cameraDepth / maxDistance;
return saturate((distance - fogStart) / (fogEnd - fogStart));
}

float calculateFogIntensityFaded(float cameraDepth, float maxDistance, float fogStart, float fogEnd, float fogAlpha) {
float distance = cameraDepth / maxDistance;
distance += fogAlpha;
return saturate((distance - fogStart) / (fogEnd - fogStart));
}

float3 applyFog(float3 diffuse, float3 fogColor, float fogIntensity) {
return mix(diffuse, fogColor, fogIntensity);
}
//#line 55 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/ActorBase.dragonfx"
void ActorVert(inout VertexInput vertInput, inout VertexOutput vertOutput) {
World = mul(World, Bones[vertInput.boneId]);
WorldView = mul(View, World);
WorldViewProj = mul(Proj, WorldView);

float2 texcoord = vertInput.texcoord0;
texcoord = applyUvAnimation(texcoord, UVAnimation);
vertInput.texcoord0 = texcoord;

float lightIntensity = calculateLightIntensity(World, float4(vertInput.normal.xyz, 0.0), TileLightColor);
lightIntensity += OverlayColor.a * 0.35;
vertOutput.light = float4(lightIntensity * TileLightColor.rgb, 1.0);
}

void ActorVertFog(StandardVertexInput vertInput, inout VertexOutput vertOutput) {
vertOutput.position = jitterVertexPosition(vertInput.worldPos);
float cameraDepth = vertOutput.position.z;
float fogIntensity = calculateFogIntensity(cameraDepth, FogControl.z, FogControl.x, FogControl.y);
vertOutput.fog = float4(FogColor.rgb, fogIntensity);
}

void ActorApplyFog(FragmentInput fragInput, StandardSurfaceInput surfaceInput, StandardSurfaceOutput surfaceOutput, inout FragmentOutput fragOutput) {
fragOutput.Color0.rgb = applyFog(fragOutput.Color0.rgb, surfaceInput.fog.rgb, surfaceInput.fog.a);
}

void ActorApplyFogOpaque(FragmentInput fragInput, StandardSurfaceInput surfaceInput, StandardSurfaceOutput surfaceOutput, inout FragmentOutput fragOutput) {
fragOutput.Color0.rgb = applyFog(fragOutput.Color0.rgb, surfaceInput.fog.rgb, surfaceInput.fog.a);
fragOutput.Color0.a = 1.0;
}

float4 getActorAlbedo(float2 uv) {
float4 albedo = getActorAlbedoNoColorChange(uv);
albedo = applyChangeColor(albedo, ChangeColor, MultiplicativeTintColor.rgb, 0.0);
return albedo;
}

//#line 6 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/ActorPatternUtil.dragonh"
void ActorVertPattern(StandardVertexInput stdInput, inout VertexOutput vertOutput) {
ActorVertFog(stdInput, vertOutput);
}


float4 getPatternAlbedo(int layer, float2 texcoord) {
float2 tex = (PatternUVOffsetsAndScales[layer].zw * texcoord) + PatternUVOffsetsAndScales[layer].xy;

float4 color = PatternColors[layer];
return bgfxTexture2D(s_MatTexture2, tex) * color;
}

float4 getBaseAlbedo(float2 texcoord) {
return bgfxTexture2D(s_MatTexture, texcoord);
}

float4 applyHudOpacity(float4 diffuse, float hudOpacity) {
diffuse.a *= hudOpacity;
return diffuse;
}

void applyPattern(in StandardSurfaceInput surfaceInput, inout float4 albedo) {
for (int i = 0; i < int(PatternCount.x); i++) {
float4 pattern = getPatternAlbedo(i, surfaceInput.texcoord0);
albedo = mix(albedo, pattern, pattern.a);
}
albedo.a = 1.0;
}

void ActorSurfPattern(in StandardSurfaceInput surfaceInput, inout StandardSurfaceOutput surfaceOutput) {
float4 albedo = getBaseAlbedo(surfaceInput.texcoord0);

applyPattern(surfaceInput, albedo);
//#line 45
albedo = applyHudOpacity(albedo, HudOpacity.x);

surfaceOutput.Albedo = albedo.rgb;
surfaceOutput.Alpha = albedo.a;
}

//#line 1 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/GlintUtil.dragonh"
float2 calculateLayerUV(const float2 origUV, const float offset, const float rotation, const float2 scale) {
float2 uv = origUV;
uv -= 0.5;

float rsin = sin(rotation);
float rcos = cos(rotation);

uv = mul(uv, float2x2(rcos, -rsin, rsin, rcos));
uv.x += offset;
uv += 0.5;

return uv * scale;
}

float4 glintBlend(const float4 dest, const float4 source) {
return float4(source.rgb * source.rgb, abs(source.a)) + float4(dest.rgb, 0.0);
}

float4 applyGlint(const float4 diffuse, const float4 layerUV, const BgfxSampler2D glintTexture, const float4 glintColor, const float4 tileLightColor) {
float4 tex1 = bgfxTexture2D(glintTexture, frac(layerUV.xy)).rgbr * glintColor;
float4 tex2 = bgfxTexture2D(glintTexture, frac(layerUV.zw)).rgbr * glintColor;

float4 glint = (tex1 + tex2) * tileLightColor;
glint = glintBlend(diffuse, glint);

return glint;
}
//#line 9 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/ActorGlintUtil.dragonh"
float4 applyActorGlintDiffuse(StandardSurfaceInput surfaceInput, float4 albedo) {
float4 diffuse = applyActorDiffuse(albedo, surfaceInput.Color, surfaceInput.light);
diffuse = applyGlint(diffuse, surfaceInput.layerUv, s_MatTexture1, GlintColor, TileLightColor);

return diffuse;
}

void ActorGlint_getAlphaColor(in StandardSurfaceInput surfaceInput, inout StandardSurfaceOutput surfaceOutput) {
float4 albedo = getActorAlbedoNoColorChange(surfaceInput.UV);

if (shouldDiscard(albedo.rgb, albedo.a, ActorFPEpsilon.x)) {
discard;
}

albedo = applyChangeColor(albedo, ChangeColor, MultiplicativeTintColor.rgb, UseAlphaRewrite.x);

float4 diffuse = applyActorGlintDiffuse(surfaceInput, albedo);

surfaceOutput.Albedo = diffuse.rgb;
surfaceOutput.Alpha = diffuse.a;
}

void ActorGlint_getTransparentColor(in StandardSurfaceInput surfaceInput, inout StandardSurfaceOutput surfaceOutput) {
float4 albedo = getActorAlbedo(surfaceInput.UV);

float4 diffuse = applyActorGlintDiffuse(surfaceInput, albedo);

surfaceOutput.Albedo = diffuse.rgb;
surfaceOutput.Alpha = diffuse.a * HudOpacity.x;
}

void ActorVertGlint(VertexInput vertInput, inout VertexOutput vertOutput) {
vertOutput.layerUv.xy = calculateLayerUV(vertInput.texcoord0, UVAnimation.x, UVAnimation.z, UVScale.xy);
vertOutput.layerUv.zw = calculateLayerUV(vertInput.texcoord0, UVAnimation.y, UVAnimation.w, UVScale.xy);

ActorVert(vertInput, vertOutput);
}

//#line 9 "D:/a/_work/3/s/handheld/src-deps/MinecraftRenderer.Materials/ActorPatternGlintUtil.dragonh"
void ActorPatternSurfGlint(in StandardSurfaceInput surfaceInput, inout StandardSurfaceOutput surfaceOutput) {
float4 outcolor = getBaseAlbedo(surfaceInput.texcoord0);


applyPattern(surfaceInput, outcolor);
//#line 20
float alpha = outcolor.a;
outcolor = applyGlint(outcolor, surfaceInput.layerUv, s_MatTexture1, GlintColor, TileLightColor);
outcolor.a = alpha;

outcolor = applyHudOpacity(outcolor, HudOpacity.x);

surfaceOutput.Albedo = outcolor.rgb;
surfaceOutput.Alpha = outcolor.a;
}

//#line 8 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Compositing.dragonh"
struct CompositingOutput {
float3 mLitColor;
};

float4 standardComposite(StandardSurfaceOutput stdOutput, CompositingOutput compositingOutput) {
return float4(compositingOutput.mLitColor, stdOutput.Alpha);
}
//#line 48 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Standard.dragontemplate"
void StandardTemplate_VertSharedTransform(VertexInput vertInput, inout VertexOutput vertOutput, out float3 worldPosition) {
//#line 58
float3 wpos = mul(World, float4(vertInput.position, 1.0)).xyz;

vertOutput.position = mul(ViewProj, float4(wpos, 1.0));
worldPosition = wpos;
}

void StandardTemplate_VertexPreprocessIdentity(VertexInput vertInput, inout VertexOutput vertOutput) {

}

void StandardTemplate_VertexOverrideIdentity(StandardVertexInput vertInput, inout VertexOutput vertOutput) {

}

void StandardTemplate_FinalColorOverrideIdentity(FragmentInput fragInput, StandardSurfaceInput surfaceInput, StandardSurfaceOutput surfaceOutput, inout FragmentOutput fragOutput) {

}

void StandardTemplate_LightingVertexFunctionIdentity(VertexInput vertInput, inout VertexOutput vertOutput, float3 worldPosition) {

}


void StandardTemplate_InvokeVertexPreprocessFunction(inout VertexInput vertInput, inout VertexOutput vertOutput);
void StandardTemplate_InvokeVertexOverrideFunction(StandardVertexInput vertInput, inout VertexOutput vertOutput);
void StandardTemplate_InvokeLightingVertexFunction(VertexInput vertInput, inout VertexOutput vertOutput, float3 worldPosition);

//#line 8 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Lighting/Common.dragonh"
struct DirectionalLight {
float3 ViewSpaceDirection;
float3 Intensity;
};
//#line 19 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Lighting/Unlit.settings.dragontemplate"
float3 computeLighting_Unlit(FragmentInput fragInput, StandardSurfaceInput stdInput, StandardSurfaceOutput stdOutput, DirectionalLight primaryLight) {
return stdOutput.Albedo;
}

//#line 128 "D:/a/_work/3/s/handheld/src-external/RenderDragon/Materials/Standard/Standard.dragontemplate"
void StandardTemplate_VertShared(VertexInput vertInput, inout VertexOutput vertOutput) {
StandardTemplate_InvokeVertexPreprocessFunction(vertInput, vertOutput);

StandardVertexInput stdInput;
stdInput.vertInput = vertInput;
StandardTemplate_VertSharedTransform(vertInput, vertOutput, stdInput.worldPos);

vertOutput.texcoord0 = vertInput.texcoord0;
vertOutput.color0 = vertInput.color0;


StandardTemplate_InvokeVertexOverrideFunction(stdInput, vertOutput);

StandardTemplate_InvokeLightingVertexFunction(vertInput, vertOutput, stdInput.worldPos);
}

void StandardTemplate_InvokeVertexPreprocessFunction(inout VertexInput vertInput, inout VertexOutput vertOutput) {
ActorVertGlint(vertInput, vertOutput);
}

void StandardTemplate_InvokeVertexOverrideFunction(StandardVertexInput vertInput, inout VertexOutput vertOutput) {
ActorVertFog(vertInput, vertOutput);
}

void StandardTemplate_InvokeLightingVertexFunction(VertexInput vertInput, inout VertexOutput vertOutput, float3 worldPosition) {
StandardTemplate_LightingVertexFunctionIdentity(vertInput, vertOutput, worldPosition);
}

void StandardTemplate_Opaque_Vert(VertexInput vertInput, inout VertexOutput vertOutput) {
StandardTemplate_VertShared(vertInput, vertOutput);
}

void StandardTemplate_Opaque_Frag(FragmentInput fragInput, inout FragmentOutput fragOutput) {

StandardSurfaceInput surfaceInput = StandardTemplate_DefaultInput(fragInput);
StandardSurfaceOutput surfaceOutput = StandardTemplate_DefaultOutput();

surfaceInput.UV = fragInput.texcoord0;
surfaceInput.Color = fragInput.color0.xyz;
surfaceInput.Alpha = fragInput.color0.a;

ActorPatternSurfGlint(surfaceInput, surfaceOutput);

DirectionalLight primaryLight;

float3 worldLightDirection = LightWorldSpaceDirection.xyz;
primaryLight.ViewSpaceDirection = mul(View, float4(worldLightDirection, 0)).xyz;
primaryLight.Intensity = LightDiffuseColorAndIlluminance.rgb * LightDiffuseColorAndIlluminance.w;

CompositingOutput compositingOutput;
compositingOutput.mLitColor = computeLighting_Unlit(fragInput, surfaceInput, surfaceOutput, primaryLight);

fragOutput.Color0 = standardComposite(surfaceOutput, compositingOutput);

ActorApplyFog(fragInput, surfaceInput, surfaceOutput, fragOutput);
}

//#line 4 "Shader.Generated.Code.Postfix"
Output main( float4 a_color0 : COLOR0 , int a_indices : BLENDINDICES , float4 a_normal : NORMAL , float3 a_position : POSITION , float2 a_texcoord0 : TEXCOORD0) { Output _varying_; _varying_.v_color0; _varying_.v_fog; _varying_.v_layerUv; _varying_.v_light; _varying_.v_texcoord0; _varying_.v_texcoords;{
//#line 5
VertexInput vertexInput;
VertexOutput vertexOutput;

vertexInput.position = (a_position);
vertexInput.boneId = (a_indices);
vertexInput.normal = (a_normal);
vertexInput.texcoord0 = (a_texcoord0);
vertexInput.color0 = (a_color0);
//#line 20
vertexOutput.texcoord0 = float2(0,0);
vertexOutput.color0 = float4(0,0,0,0);
vertexOutput.texcoords = float4(0,0,0,0);
vertexOutput.layerUv = float4(0,0,0,0);
vertexOutput.light = float4(0,0,0,0);
vertexOutput.fog = float4(0,0,0,0);
vertexOutput.position = float4(0,0,0,0);

ViewRect = u_viewRect;
Proj = u_proj;

View = u_view;



ViewTexel = u_viewTexel;
InvView = u_invView;
InvProj = u_invProj;
ViewProj = u_viewProj;

InvViewProj = u_invViewProj;
{
WorldArray[0] = u_model[0];
WorldArray[1] = u_model[1];
WorldArray[2] = u_model[2];
WorldArray[3] = u_model[3];
}

World = u_model[0];
WorldView = u_modelView;
WorldViewProj = u_modelViewProj;
AlphaRef4 = u_alphaRef4;
AlphaRef = u_alphaRef4.x;
//#line 72
StandardTemplate_Opaque_Vert(vertexInput, vertexOutput);

_varying_.v_texcoord0 = vertexOutput.texcoord0;
_varying_.v_color0 = vertexOutput.color0;
_varying_.v_texcoords = vertexOutput.texcoords;
_varying_.v_layerUv = vertexOutput.layerUv;
_varying_.v_light = vertexOutput.light;
_varying_.v_fog = vertexOutput.fog;

_varying_.gl_Position = vertexOutput.position;
} return _varying_;
//#line 83
}
